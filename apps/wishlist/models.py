from django.db import models
import bcrypt

class UserManager(models.Manager):
    def validate_user(self, form):
        errors = []

        if len(form['name']) < 3:
            errors.append("Name must be at least 3 characters long")
        if len(form['username']) < 3:
            errors.append("Username must be at least 3 characters long")
        if len(form['password']) < 8:
            errors.append("Password must be at least 8 characters long")        
        if form['password'] != form['confirm_pw']:
            errors.append("Passwords do not match")

        try:
            self.get(username=form['username'])
            errors.append("Username is already in use")
        except:
            pass

        return errors

    def create_user(self, user_data):
        pw_hash = bcrypt.hashpw(user_data['password'].encode(), bcrypt.gensalt())
        user = self.create(
            name=user_data['name'],
            username=user_data['username'],
            pw_hash=pw_hash,
            date_hired=user_data['date_hired']
        )
        return user
    
    def login(self, form):
        try:
            user = self.get(username=form['username'])
            if bcrypt.checkpw(form['password'].encode(), user.pw_hash.encode()):
                return(True, user.id)
            else:
                return(False, "Invalid username/password combination")
        except:
            return(False, "Account does not exist")

class User(models.Model):
    name = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    pw_hash = models.CharField(max_length=255)
    date_hired = models.DateTimeField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

class ItemManager(models.Manager):
    def validate_item(self, form):
        errors = []
        if form['name'] == "":
            errors.append("Please enter an item")
        return errors

    def create_item(self, user_data, id):
        self.create(name=user_data['name'], user=User.objects.get(id=id))
    def join(self, item_id, user_id):
        self.get(id=item_id).join.add(user_id)
    def delete(self, item_id, user_id):
        self.get(id=item_id).join.remove(user_id)
    
class Item(models.Model):
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    join = models.ManyToManyField(User, related_name="Users")
    user = models.ForeignKey(User, related_name="items")
    objects = ItemManager()