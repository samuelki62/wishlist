from django.shortcuts import render, HttpResponse, redirect
from django.contrib import messages
from .models import User, Item

def index(request):
    if 'id' in request.session:
        return redirect('/dashboard')
    return render(request, 'wishlist/index.html')

def register(request):
    if request.method != 'POST':
        return redirect('/')

    errors = User.objects.validate_user(request.POST)
    if len(errors) > 0:
        for error in errors:
            messages.error(request, error)
        return redirect('/')
    else:
        user = User.objects.create_user(request.POST)
        request.session['id'] = user.id
        return redirect('/dashboard')

def login(request):
    if request.method != 'POST':
        return redirect('/')

    valid, response = User.objects.login(request.POST)
    if valid == True:
        request.session['id'] = response
        return redirect('/dashboard')
    else:
        messages.error(request, response)
        return redirect('/')

def logout(request):
    request.session.clear()
    return redirect('/')

def dashboard(request):
    if 'id' not in request.session:
        return redirect('/')

    context = {
        'user': User.objects.get(id=request.session['id']),
        'myitems': Item.objects.filter(user=User.objects.get(id=request.session['id'])),
        'added': Item.objects.filter(join=User.objects.filter(id=request.session['id'])),
        'allitems': Item.objects.exclude(user=User.objects.get(id=request.session['id'])).exclude(join=User.objects.filter(id=request.session['id']))
    }
    return render(request, 'wishlist/dashboard.html', context)

def add(request):
    if 'id' not in request.session:
        return redirect('/')
    return render(request, 'wishlist/create_item.html')

def create(request):
    if 'id' not in request.session:
        return redirect('/')

    errors = Item.objects.validate_item(request.POST)
    if len(errors) > 0:
        for error in errors:
            messages.error(request, error)
        return redirect('/add')
    else:
        Item.objects.create_item(request.POST, request.session['id'])
        return redirect('/dashboard')

def show(request, id):
    if 'id' not in request.session:
        return redirect('/')

    context = {
        'items': Item.objects.filter(id=id)
    }
    return render(request, 'wishlist/show_item.html', context)

def delete(request, id):
    Item.objects.get(id=id).delete()
    return redirect('/dashboard')

def remove(request, id):
    Item.objects.delete(id, request.session['id'])
    return redirect('/dashboard')

def join(request, id):
    Item.objects.join(id, request.session['id'])
    return redirect('/dashboard')