from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^main$', views.index, name="index"),
    url(r'^register$', views.register, name="register"),
    url(r'^login$', views.login, name="login"),
    url(r'^logout$', views.logout, name="logout"),
    url(r'^dashboard$', views.dashboard, name="dashboard"),
    url(r'^add$', views.add, name="additem"),
    url(r'^wish_items/create$', views.create, name="createitem"),
    url(r'^wish_items/(?P<id>\d+)$', views.show, name="show"),
    url(r'^wish_items/(?P<id>\d+)/delete$', views.delete, name="delete"),
    url(r'^remove/(?P<id>\d+)$', views.remove, name="remove"),
    url(r'^join/(?P<id>\d+)$', views.join, name="join")
]
